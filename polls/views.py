from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, Http404
from .models import Question
# from django.template import loader
# Create your views here.

# def index(request):
#     lastest_question_list = Question.objects.order_by('-pub.date')[:5]
#     ouput = ', '.join([q.question_text for q in lastest_question_list])
#     return HttpResponse(ouput)


# def index(request):
#     latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     template = loader.get_template('polls/index.html')
#     context = {'latest_question_list': latest_question_list,}
#     return HttpResponse(template.render(context, request))

# Un contexte est un dictionnaire qui fait correspondre des objets Python à des noms de variables de gabarit.

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list, }
    return render(request, 'polls/index.html', context)


# def detail(request, question_id):
#     try:
#         question = Question.objects.get(pk=question_id)
#     except Question.DoesNotExist:
#         raise Http404("Question not exists")
#     return render(request, "polls/details.html", {question_id})

def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/details.html", {'question': question})


def results(request, question_id):
    response = f"You're looking at the resultat of quesytion {question_id}"
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You're voting on question {} ".format(question_id))


